let first = new Map([
    [1, 'one'],
    [2, 'two'],
    [3, 'three'],
  ])
  
  let second = new Map([
    [1, 'uno'],
    [2, 'dos']
  ])
  
  
  let merged = new Map([...first, ...second, [1, 'eins']])
  
  console.log(merged.get(1)) 
  console.log(merged.get(2)) 
  console.log(merged.get(3)) 
  console.log(merged);